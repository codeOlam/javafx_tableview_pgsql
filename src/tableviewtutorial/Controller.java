package tableviewtutorial;



import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author codeoalm
 */
public class Controller implements Initializable {
    
    //******************* init of db parameters ******************
    private dbUtils db;
    private Connection con_db;

    
    //************* init observableArray *******************
    private ObservableList<TableModel> data;
    
    //***************************************************************//
    // Initialization of fxml parameters                             //
    //***************************************************************//
    
    @FXML
    private TableView<TableModel> db_table;

    @FXML
    private TableColumn<TableModel, Integer> col_id;

    @FXML
    private TableColumn<TableModel, String> col_fname;

    @FXML
    private TableColumn<TableModel, String> col_lname;

    @FXML
    private TableColumn<TableModel, String> col_email;

    @FXML
    private TableColumn<TableModel, String> col_createdAt;

    @FXML
    private Button btn_load_db;

    
    //***************************************************************//
    // Function for fmxl click actions                              //
    //***************************************************************//
    @FXML
    void handleButtonAction(ActionEvent event) {
        if(event.getSource() == btn_load_db){
            load_db();
        }

    }// End function handle clicks
    
    
    
    
    //******************** Function to load *************************
    public void load_db(){
        try {
            /*This function load data from the database
            */
            db.insertData();
            

            
            // creating the observablelist array object
            data = FXCollections.observableArrayList();
            
            //Query database table
            String query = "SELECT * FROM test_table";
            // Execute query and store result in resultset
            ResultSet rs = con_db.createStatement().executeQuery(query);
            
            //Loop throw result set and populate table clumns and rows with values from rs.
            while(rs.next()){
                data.add(new TableModel(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4), 
                        rs.getString(5)));
                
            }
            
            System.out.println("The value of data: \n"+ data);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("Could not load db");
        }
        
        
        //Added data to UI table colmns and rows.
        col_id.setCellValueFactory(new PropertyValueFactory<>("staffId"));
        col_fname.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_lname.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        col_createdAt.setCellValueFactory(new PropertyValueFactory<>("createdAt"));

        db_table.setItems(null);
        db_table.setItems(data);
        
    }// End function load_db()


    
    //********************* Main Function begins ********************//
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        db = new dbUtils();
        con_db = db.dbConnection(); //init con_db
    }    
    
}
